## Date and time
31st August, 2015
Location: Reading Room, IIIT-Hyderabad

## Attendees
Client: Mr.Girish Sarwate
Team Members: Isha Mangurkar, Divanshu Jain, Natha Avinash, Thatavarthi Phanindra Kumar

## Agenda
Discus progress of the project.

## Minutes
### Discussion
*(whatever the clients has explained, what all doubts students asked, and solutions proposed.)*
* Client showed the SketchUp API tool that was to be used for measuring the length of objects in google video inputs.
 * The tool is a measuring tool that returns the value of a length of an object.
 * This tool is to be used in 360 camera input to calculate length of structures such as a pole, wall, etc.
 * It returns the value of length.
* The end user of this project application is supposed to be civil engineering students.
* Its supposed to be a stand-alone application.

### Action Points
*(Tasks identified to be completed by next meeting.)*
* Find out if any emails sent out before week- one meeting have been misplaced.
* Begin work on understanding the API's based on tutorials.
* Fix timings for weekly meetings/ fortnight meetings for updates on project progress.

## Date of the next meeting
 12th September 2015, 6:30 pm.
 Location: Reading room, IIIT-Hyderabad
